import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { MainViewComponent } from './components/views/main/main-view.component';

@NgModule( {
	declarations: [
		AppComponent,
		MainViewComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		AppRoutingModule
	],
	providers: [],
	bootstrap: [ AppComponent ]
} )
export class AppModule {
}
