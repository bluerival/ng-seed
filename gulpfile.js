'use strict';

const fs = require( 'fs' );
const gulp = require( 'gulp' );
const gulpAutoprefixer = require( 'gulp-autoprefixer' );
const gulpCached = require( 'gulp-cached' );
const gulpDebug = require( 'gulp-debug' );
const gulpInsert = require( 'gulp-insert' );
const gulpLess = require( 'gulp-less' );
const gulpPlumber = require( 'gulp-plumber' );
const gulpPug = require( 'gulp-pug' );
const gulpTypescript = require( 'gulp-typescript' );
const gulpWatch = require( 'gulp-watch' );
const gulpWatchLess = require( 'gulp-watch-less' );
const gulpWatchPug = require( 'gulp-watch-pug' );

const generateTypescriptTask = function ( watch ) {

	let config = {};

	try {
		let tempConfig = fs.readFileSync( './tsconfig.json' );

		if ( tempConfig && tempConfig.toString ) {
			tempConfig = JSON.parse( tempConfig.toString( 'utf8' ) );

			if ( tempConfig && tempConfig.compilerOptions ) {
				config = tempConfig.compilerOptions;
			}

		}

	} catch ( e ) {
		console.error( 'failed to load tsconfig.json', e );
		process.exit( 1 );
	}

	let src = 'src/assets/scripts/**/*.ts';

	config = {
		noImplicitAny: true,
		sourceMap: config.sourceMap || true,
		target: config.target || 'es5',
		typeRoots: config.typeRoots || [
			"node_modules/@types"
		],
		lib: config.lib || [
			"es2016",
			"dom"
		]
	};

	let go = () => {

		return gulp.src( src )
			.pipe( gulpCached( 'typescript' ) )
			.pipe( gulpTypescript( config ) )
			.pipe( gulpDebug( { title: 'Typescript:' } ) )
			.pipe( gulpInsert.prepend( `/* WARNING: This is a generated file. Please edit the corresponding .ts file. */\n\n` ) )
			.pipe( gulp.dest( 'src/assets/scripts' ) );

	};

	let result = go();

	if ( watch ) {
		result = gulpWatch( src, () => {
			go();
		} );
	}

	return result;

};

const generatePugTask = function ( watch ) {

	let src = 'src/**/*.pug';

	let task = watch ? gulpWatch( src, { ignoreInitial: false } ).pipe( gulpWatchPug( src, { delay: 1000 } ) ) : gulp.src( src );

	task = task
		.pipe( gulpCached( 'pug' ) )
		.pipe( gulpPug( {
			pretty: true
		} ) )
		.pipe( gulpDebug( { title: 'Pug:' } ) )
		.pipe( gulp.dest( 'src' ) );

	return task;

};

const generateLessTask = function ( watch ) {

	let src = 'src/**/*.less';

	let task = gulp.src( src );

	if ( watch ) {
		task = task
		// .pipe( gulpWatch( src ) )
			.pipe( gulpWatchLess( src, { delay: 1000 } ) );
	}

	task = task
		.pipe( gulpInsert.prepend( '@import (reference) "src/styles/config.less";' ) )
		.pipe( gulpPlumber( {
			errorHandler: function ( err ) {
				console.log( 'err', err && err.message || 'Unknown error processing less files.' );
				this.emit( 'end' );
				process.exit( 1 );
			}
		} ) )
		.pipe( gulpLess( {
			paths: [
				__dirname + '/src'
			]
		} ) )
		.pipe( gulpPlumber.stop() )
		.pipe( gulpAutoprefixer() )
		.pipe( gulpCached( 'less' ) ) // this avoids triggering other downstream builds if un-needed
		.pipe( gulpDebug( { title: 'Less:' } ) )
		.pipe( gulpInsert.prepend( `/* WARNING: This is a generated file. Please edit the corresponding .less file. */\n\n` ) )
		.pipe( gulp.dest( 'src' ) );

	return task;

};

gulp.task( 'pug', function () {
	return generatePugTask( false );
} );
gulp.task( 'pug-watch', function () {
	return generatePugTask( true );
} );

gulp.task( 'less', function () {
	return generateLessTask( false );
} );
gulp.task( 'less-watch', function () {
	return generateLessTask( true );
} );

gulp.task( 'typescript', function () {
	return generateTypescriptTask( false );
} );
gulp.task( 'typescript-watch', function () {
	return generateTypescriptTask( true );
} );
