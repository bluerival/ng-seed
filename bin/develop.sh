#!/bin/bash

# catch non ctrl+c exits and ensure child processes exit
function cleanup() {
	kill -- "-$$"
	wait $(jobs -p)
}
trap cleanup TERM

trap "wait; echo 'All jobs completed. Exiting.'" EXIT

echo "Static file server run with sudo. Please provide your password to approve."
echo
sudo echo "Password accepted"

if [ $? -ne 0 ]; then
	echo
	echo "We can not run the static development server without sudo access."
	echo
	exit 1
fi

echo
echo "Please wait while we initialize. You will be ready for development once the index.html file shows its first render."

rm -rf ./build/dev
wait

function startGulp() {

	TASK="${1}"

	/bin/bash bin/gulpWatchDaemon.sh "${TASK}" &

	sleep 1

}

# run these processed in parallel

# ts -> js compiling
startGulp "typescript-watch"

# pug -> html compiling
startGulp "pug-watch"

# less -> css compiling
startGulp "less-watch"

# angular CLI build
ng build --target=development --env=dev --output-path=build/dev --sourcemap --output-hashing=none --progress --watch &
sleep 1

# generic, super simple static file server
sudo node server.js &
sleep 1

# don't return to the terminal while jobs are running
wait

