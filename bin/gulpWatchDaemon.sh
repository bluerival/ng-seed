#!/bin/bash

TASK="${1}"

#trap 'echo "Shutting down gulp watch ${TASK}"' EXIT

while [ true ]; do

	gulp "${TASK}"
	EXIT_CODE=$?

	if [ "${EXIT_CODE}" = "1" ]; then
		echo "Restarting gulp watch ${TASK} in 3 seconds..."
		sleep 3;
	else
		exit 0;
	fi

done
