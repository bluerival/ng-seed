#!/bin/bash

export DEPLOY_ENV="$1"

if [ "${DEPLOY_ENV}" = "" ]; then
	echo "Must supply environment to deploy to"
	exit;
fi

echo "Deploying to ${DEPLOY_ENV}"
echo

DEPLOY_PATH="./build/deploy-${DEPLOY_ENV}"

rm -rf "${DEPLOY_PATH}"

echo "Building typescript files"
gulp typescript
echo

echo "Building less files"
gulp less
echo

echo "Building pug files"
gulp pug
echo

echo "Compiling Angular App"
ng build --target=production --env="${DEPLOY_ENV}" --output-path="${DEPLOY_PATH}" --sourcemap --aot --output-hashing=all --progress
echo

echo "Cleaning up less, ts and/or pug files that ended up in deploy path"
find "${DEPLOY_PATH}" -name \*.ts -delete
find "${DEPLOY_PATH}" -name \*.less -delete
find "${DEPLOY_PATH}" -name \*.pug -delete
echo

echo "Complete"
