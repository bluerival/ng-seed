# ng-seed

This is a seed application for an Angular 4.0 development project. The project was initially generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0, using default parameters. It has since been customized with support for Pug templates, ng-bootstrap library added, enhanced Less support added and some development and deployment scripts described below.

## Development server

Run `/bin/bash bin/develop.sh` for a dev server. Navigate to `https://localhost/`. It is recommended you edit your hosts file to use a real domain name aliases to localhost/127.0.0.1 rather than localhost. Using a real domain name instead of localhost avoids some unusual issues with browser security, especially with CORS. 

Also, the development files are built similarly to a deploy, except development builds omit AoT compiling, so that raw html, css and Javascript can still be accessed in the browser. The files, which are hosted by a basic static web server, are located in `build/dev`.

#### Pug

The development command will automatically compile Pug to HTML. Any .pug files located in the directory tree under `src/` will get automatically compiled. The corresponding HTML file will be along side the Pug file.

#### Less

The development command will automatically compile Less to CSS. Any .kess files located in the directory tree under `src/` will get automatically compiled. The corresponding CSS file will be along side the Less file.

Also, note that the Less compiler will automatically pre-pend an import to `src/styles/config.less` as a reference on every Less file compiled. And, the config file in turn imports `src/styles/theme.less`. These files will not impact Bootstrap styles, however, as we have yet to integrate the Less compiler with the Angular CLI framework for Bootstrap's Less file compiling. 

Note: Although Angular CLI natively supports compiling Less, we opted to use our own Gulp version as we can control the process better, applying compilation patterns not currently exposed by Angular CLI.


## Code scaffolding

Run `ng generate component <component-name>` to generate a new component in your current directory. You can also use `ng generate <directive/pipe/service/class/module>`.

## Build for Deployment

Run `/bin/bash bin/deploy.sh <environment>` to build the project for the specified environment. The build artifacts will be stored in the `build/deploy-<environment>` directory. The contents of `build/deploy-<environment>` are suitable for copying directly up to a CDN or static file server. Simply configure your CDN or static file server to load the `index.html` file for any supported routes you wish to use for your app.
 
Note: Any existing contents of `build/deploy-<environment>` will be deleted immediately, and without warning, upon running this command. 

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
