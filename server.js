'use strict';

const compression = require( 'compression' );
const express = require( 'express' );
const fs = require( 'fs' );
const https = require( 'https' );
const http = require( 'http' );

const app = express();

app.use( compression() );
app.use( ( req, res, next ) => {

	let entry = {
		protocol: req.protocol.toUpperCase(),
		hostname: req.hostname,
		path: req.path,
		method: req.method.toUpperCase(),
		ip: req.ip,
		ips: req.ips,
		cookies: null,
		query: null
	};

	let startTime = new Date().getTime();

	res.once( 'finish', () => {

		let endTime = new Date().getTime();

		console.log( `${new Date().toISOString()}: ${endTime - startTime}ms: ${entry.hostname}: ${res.statusCode}: ${entry.protocol} ${entry.method} ${entry.path}` );

	} );

	next();

} );

app.use(
	express.static( 'build/dev', { fallthrough: true } ),
	( req, res ) => {

		let index = __dirname + '/build/dev/index.html';
		res.sendFile( index );

	}
);

http.createServer( app ).listen( 80, ( err ) => {
	if ( err ) {
		console.log( 'failed to open port 80' );
		process.exit();
	} else {
		console.log( 'HTTP listening on port 80' );
	}
} );

https.createServer( {
	dhparam: fs.readFileSync( 'dev/tls/dhparam.pem' ),
	key: fs.readFileSync( 'dev/tls/key.pem' ),
	cert: fs.readFileSync( 'dev/tls/cert.pem' )
}, app ).listen( 443, ( err ) => {
	if ( err ) {
		console.log( 'failed to open port 443' );
		process.exit();
	} else {
		console.log( 'HTTPS listening on port 443' );
	}
} );
